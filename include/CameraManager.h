
#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class CameraManager
{
public:
	CameraManager();
	~CameraManager();

	void	create( SceneManager* sceneManager, RenderWindow* renderWindow );
	void	update( float dtimeInSeconds );
	
	Camera* getCamera();
	void moveCamera( Vector3 goto_pos );	// moves the camera ground node in a smooth motion to the desired goto_pos

private:
	Root*			root_;
	SceneManager*	sceneManager_;
	RenderWindow*	renderWindow_;
	Camera*			camera_;
	Viewport*		viewport_;
	SceneNode*		groundNode;		// ground node of which the camera node is a child node
	SceneNode*		cameraNode;		// node to which the camera is attached

	Vector3		goto_pos_;			// position to which the camera will smoothly move over time
	Vector3		camera_velocity_;	// the velocity of the camera

	void updateCameraPosition( float dtimeInSeconds );
};


#endif // CAMERAMANAGER_H