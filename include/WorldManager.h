
#ifndef WORLDMANAGER_H
#define	WORLDMANAGER_H

// forward declarations
class GUIManager;
class StateManager;
class Character;

// including headers
#include <Ogre.h>
#include "CameraManager.h"
#include "Pathfinder.h"

// using namespaces
using namespace Ogre;

class WorldManager
{
public:
	WorldManager();
	~WorldManager();

	void create( Root* root, RenderWindow* renderWindow, GUIManager& guiManager, StateManager& stateManager );
	void update( float dtimeInSeconds );

	// Input events
	bool mouseLeftDownEvent();
	bool mouseRightDownEvent();
	bool mouseMovedEvent( float xPos, float yPos );
	bool mouseScrollEvent( short scroll );
	bool keyWPressedEvent();
	bool keyAPressedEvent();
	bool keySPressedEvent();
	bool keyDPressedEvent();

private:
	Root*			root_;
	RenderWindow*	renderWindow_;
	GUIManager*		guiManager_;
	StateManager*	stateManager_;
	SceneManager*	sceneManager_;
	CameraManager	cameraManager_;
	
	// World creation
	std::vector< StringVector >		worldMap_;
	ManualObject*					worldGrid_;					// world grid object
	float							gridStep_;					// size of the grid
	short							gridWidth_, gridLength_;	// width (x) and length (z) of the grid in grid blocks
	
	std::vector< Character* >	team_vec_;				// vector of pointers to all team characters
	
	void loadWorldData( const char* worldmap_fileName );
	void createWorldGeometry();
	void createWorldGrid( ushort length, ushort width, float step );

	// Pathfinding
	Pathfinder									pathfinder_;
	std::vector< std::pair< short, short > >	shortestPath_vec_;
	std::vector< SceneNode* >					shortestPath_SceneNodes_;
	SceneNode*									gridNode_pointto_;			// scene node at the grid to which the mouse is pointing
	SceneNode*									gridNode_selection_;		// scene node at the grid which is selected
	Entity*										gridEnt_pointto_;			// entity located at gridNode_pointto_
	Entity*										gridEnt_selection_;			// entity located at gridNode_selection_

	void updateShortestPath( const std::pair< short, short >* from_pos, const std::pair< short, short >* to_pos );	// if one _pos = 0, remove path
};

#endif // WORLDMANAGER_H
