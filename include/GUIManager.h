
#ifndef GUIMANAGER_H
#define GUIMANAGER_H

// forward declarations
class WorldManager;
class StateManager;

// including headers
#include <Ogre.h>
#include <CEGUI/CEGUI.h>

// using namespaces
using namespace Ogre;

class GUIManager
{
public:
	GUIManager();
	~GUIManager();

	void create( WorldManager& worldManager, StateManager& stateManager, ushort video_width, ushort video_height, bool bVideo_fullscreen );
	CEGUI::Window* getWindowFromString( const String& window_str );
	bool isMouseVisible();
	void openMainMenu();	
	void setVideoMode( bool bFullscreen, ushort width, ushort height );
	void setWindowText( CEGUI::Window* window, const String& text );
	void showWindowInSeconds( CEGUI::Window* window, float secs );		// secs <= 0 -> infinite time
	void update( float dtimeInSeconds );

	// Input events
	bool mouseLeftDownEvent();
	bool mouseLeftUpEvent();
	bool mouseRightDownEvent();
	bool mouseRightUpEvent();
	bool mouseMovedEvent( float xPos, float yPos );
	bool mouseScrollEvent( short scroll );
	
	// CEGUI events
	bool mainMenuReturnEvent( const CEGUI::EventArgs& args );
	bool mainMenuQuitEvent( const CEGUI::EventArgs& args );

private:
	WorldManager*			worldManager_;
	StateManager*			stateManager_;
	CEGUI::System*			ceguiSystem_;
	CEGUI::WindowManager*	windowManager_;

	// Windows and menus
	CEGUI::Window* rootWindow_;
	CEGUI::Window* mainMenu_;
	CEGUI::Window* gameplayMenu_;

	std::map<CEGUI::Window*,float>				tempWindows_;
	std::map<CEGUI::Window*,float>::iterator	tempWindowsIt_;

	void loadWindowLayouts();
	void setMousePosition( float xPos, float yPos );
	void setMouseVisibility( bool bShow );
	void updateTempWindows( float dtimeInSeconds );
};

#endif // GUIMANAGER_H
