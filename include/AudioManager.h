
#ifndef AUDIOMANAGER_H
#define AUIOMANAGER_H

//including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class AudioManager
{
public:
	AudioManager();
	~AudioManager();

	void create( RenderWindow* renderWindow );	// creates the object
};

#endif // AUDIOMANAGER_H