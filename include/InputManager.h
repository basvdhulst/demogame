
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

// forward declarations
class GUIManager;
class WorldManager;
class StateManager;

// including headers
#include <Ogre.h>
#include <OISInputManager.h>
#include <OISMouse.h>
#include <OISKeyboard.h>

// using namespaces
using namespace Ogre;

class InputManager : public OIS::MouseListener, OIS::KeyListener
{
public:
	InputManager();
	~InputManager();

	void create( RenderWindow* renderWindow, GUIManager& guiManager, WorldManager& worldManager, StateManager& stateManager );
	void update();
	void updateMouseWindow();

private:
	RenderWindow*		renderWindow_;
	GUIManager*			guiManager_;
	WorldManager*		worldManager_;
	StateManager*		stateManager_;
	OIS::InputManager*	oisInputManager_;
	OIS::Mouse*			mouse_;
	OIS::Keyboard*		keyboard_;

	static const short MAX_KEYS_ = 144;
	short keyMap_[MAX_KEYS_];				// key map, can be altered to accomplish key-remapping
		
	// input events
	bool mouseMoved		( const OIS::MouseEvent &evt );
	bool mousePressed	( const OIS::MouseEvent &evt, OIS::MouseButtonID btn );
	bool mouseReleased	( const OIS::MouseEvent &evt, OIS::MouseButtonID btn );
	bool keyPressed		( const OIS::KeyEvent &evt );
	bool keyReleased	( const OIS::KeyEvent &evt );
};

#endif // INPUTMANAGER_H
