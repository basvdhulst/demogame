
#ifndef TIMEMANAGER_H
#define TIMEMANAGER_H

// forward declarations
//class DebugManager;
class StateManager;
class GUIManager;
class WorldManager;
class InputManager;

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class TimeManager
{
public:
	TimeManager();
	~TimeManager();

	void create( Root* root, StateManager& stateManager, GUIManager& guiManager, WorldManager& worldManager, InputManager& inputManager );
	void runRenderLoop();

private:
	Root*			root_;
	//DebugManager*	debugManager_;
	StateManager*	stateManager_;
	GUIManager*		guiManager_;
	WorldManager*	worldManager_;
	InputManager*	inputManager_;
	Timer*			timer_;
};

#endif // TIMEMANAGER_H
