
#ifndef STATEMANAGER_H
#define STATEMANAGER_H

// forward declarations
class InputManager;

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

typedef enum
{
	// Note that the states must be chosen such that NEVER two states can be active at the same time!
	STARTUP,
	SHUTDOWN
}	State;

class StateManager
{
public:
	StateManager();
	~StateManager();

	void	create( RenderSystem* renderSystem, RenderWindow* renderWindow, InputManager& inputManager );

	State	getCurrentState();
	ushort	getScreenWidth();
	ushort	getScreenHeight();
	bool	isFullscreen();
	void	setVideoMode( bool bFullscreen, ushort width, ushort height );
	void	shutDown();

private:
	RenderSystem*	renderSystem_;
	RenderWindow*	renderWindow_;
	InputManager*	inputManager_;
	State			currentState_;

	ushort videoWidth_;
	ushort videoHeight_;

	bool	requestStateChange( State newState );
};

#endif // STATEMANAGER_H