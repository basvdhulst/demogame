
#ifndef CHARACTER_H
#define CHARACTER_H

// including headers
#include "Organism.h"

class Character : public Organism
{
public:
	Character( const String& name = "Stranger", const String& portret = "portrets/unknown_character" );
	~Character();

	void create( SceneManager* sceneManager, const String& ent_str, const Vector3& pos_vec );

private:
	bool player_control;	// only true if player is to control character
};


#endif // CHARACTER_H