
#ifndef PATHFINDER_H
#define PATHFINDER_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class Pathfinder
{
public:
	Pathfinder();
	~Pathfinder();

	bool findShortestPath(	const std::vector< StringVector >& worldMap, const std::pair< short, short >& start_pos,
							const std::pair< short, short >& goal_pos, std::vector< std::pair< short, short > >& path_vec );

private:

	// returns the map value, or -1 if node is not on the map, or non-integer, or negative
	short getMapValue( const std::vector< StringVector >& worldMap, const std::pair< short, short >& pos );
	// get the distance between pos1 and pos2 for normalized grid spacing
	float getDistance( const std::pair< short, short >& pos1, const std::pair< short, short >& pos2 );
	// returns the vector pointer, now filled with the shortest path
	std::vector< std::pair< short, short > >& reconstruct_path(	const std::map< std::pair< short, short >, std::pair< short, short > >& camefrom,
																const std::pair< short, short >& pos,
																std::vector< std::pair< short, short > >& path_vec );
};

#endif // PATHFINDER_H