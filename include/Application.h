
#ifndef APPLICATION_H
#define APPLICATION_H

// including headers
#include <Ogre.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>
//#include "DebugManager.h"
#include "GUIManager.h"
#include "WorldManager.h"
#include "AudioManager.h"
#include "StateManager.h"
#include "InputManager.h"
#include "TimeManager.h"

// using namespaces
using namespace Ogre;

class Application
{
public:
	Application();
	~Application();

	void create();
	void run();

private:
	Root*					root_;
	RenderSystem*			renderSystem_;
	RenderWindow*			renderWindow_;
	CEGUI::OgreRenderer*	ogreRenderer_;
	
	// Factory classes
	//DebugManager	debugManager_;
	GUIManager		guiManager_;
	WorldManager	worldManager_;
	AudioManager	audioManager_;
	StateManager	stateManager_;
	InputManager	inputManager_;
	TimeManager		timeManager_;

	ushort	video_width_;
	ushort	video_height_;
	bool	bVideo_fullscreen_;
	
	void initializePlugins();
	void initializeRenderSystem();
	void initializeRenderWindow();
	void initializeResources();
	void initializeCEGUI();
};

#endif // APPLICATION_H
