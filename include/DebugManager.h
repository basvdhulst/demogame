
#ifndef DEBUGMANAGER_H
#define DEBUGMANAGER_H

// including headers
#include <Ogre.h>
#include <CEGUI/CEGUI.h>

// using namespaces
using namespace Ogre;

class DebugManager : public Singleton< DebugManager >
{
public:
	DebugManager();
	~DebugManager();

	void create();
	void update( float dtimeInSeconds );

	void printToConsole( const String& str );

private:
	CEGUI::WindowManager*	windowManager_;
	CEGUI::Window*			rootWindow_;
	CEGUI::Window*			debugWindow_;

	double	expiredTimeInSeconds_;
	float	timeSinceFPSUpdate_;
	uint16	framesSinceFPSUpdate_;
};

#endif // DEBUGMANAGER_H
