
#ifndef ORGANISM_H
#define ORGANISM_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class Organism
{
public:
	Organism( const String& name = "Unknown creature", const String& portret = "portrets/unknown_organism" );
	~Organism();

	virtual void	create( SceneManager* sceneManager, const String& ent_str, const Vector3& pos_vec );		// creates the entity
	const String&	getName();
	const String&	getPortret();
	const Vector3&	getPosition();
	void			setScale( const Vector3& scale_vec );		// scales the sceneNode_

private:
	Entity*		entity_;			// the model of the organism
	SceneNode*	sceneNode_;			// sceneNode to which the entity is attached
	String		name_;				// displayed name of the character
	String		portret_;			// name of the portret material
};


#endif // ORGANISM_H