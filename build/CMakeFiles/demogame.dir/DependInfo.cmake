# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/Application.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/Application.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/AudioManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/AudioManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/CameraManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/CameraManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/Character.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/Character.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/GUIManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/GUIManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/InputManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/InputManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/Organism.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/Organism.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/Pathfinder.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/Pathfinder.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/StateManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/StateManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/TimeManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/TimeManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/WorldManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/WorldManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/src/main.cpp" "/home/bas/Documents/Programmeren/Projects/C++/DemoGame/build/CMakeFiles/demogame.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/ois"
  "/usr/local/include/OGRE"
  "/usr/local/include/cegui-0"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
