************************
Demo Game (2011)
************************

Movement of player by mouse click by shortest path, using the A* 
pathfinding algorithm. World defined by 2D grid.

Libraries: Ogre3D, OIS, CEGUI, BASS

NOTE: Project was made in Windows with older version of Ogre. At this 
time I was unsuccessful in converting the files to Linux, Ogre v1-9 
and CEGUI v0-8 (black screen).
