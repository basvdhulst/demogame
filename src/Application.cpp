
// including headers
#include "Application.h"



Application::Application() :
	root_(0),
	renderSystem_(0),
	renderWindow_(0),
	ogreRenderer_(0)
{
}



Application::~Application()
{
	ogreRenderer_->destroySystem();
	delete root_;
}



void Application::create()
{
	root_ = new Root("","");

	initializePlugins();
	initializeRenderSystem();
	initializeRenderWindow();
	initializeResources();
	initializeCEGUI();

	//debugManager_	.create();
	stateManager_	.create( renderSystem_, renderWindow_, inputManager_ );
	guiManager_		.create( worldManager_, stateManager_, video_width_, video_height_, bVideo_fullscreen_ );
	worldManager_	.create( root_, renderWindow_, guiManager_, stateManager_ );
	//audioManager_	.create( renderWindow_ );
	inputManager_	.create( renderWindow_, guiManager_, worldManager_, stateManager_ );
	timeManager_	.create( root_, stateManager_, guiManager_, worldManager_, inputManager_ );	
}



void Application::run()
{
	timeManager_.runRenderLoop();
}



void Application::initializePlugins()
{
	ConfigFile cf;
	
	#ifdef _DEBUG
		cf.load( "plugins_d.ini" );
	#else
		cf.load( "plugins.ini" );
	#endif

	ConfigFile::SectionIterator seci = cf.getSectionIterator();
	ConfigFile::SettingsMultiMap *set = seci.getNext();
	ConfigFile::SettingsMultiMap::iterator i;
	
	for (i = set->begin(); i != set->end(); ++i)
		root_->loadPlugin( i->second );
}



void Application::initializeRenderSystem() 
{
	RenderSystemList list = root_->getAvailableRenderers();
	RenderSystemList::iterator i = list.begin();

	if(i != list.end() && !renderSystem_) {
		renderSystem_ = *i;
		root_->setRenderSystem( renderSystem_ );
	}

	if(root_->getRenderSystem() == 0)
		throw Exception( 1, "Error: No render system available", "" );
	else
		root_->initialise( false );
}



void Application::initializeRenderWindow()
{
	ConfigFile cf;
	cf.load( "wndconf.ini" );
	ConfigFile::SectionIterator i = cf.getSectionIterator();
	ConfigFile::SettingsMultiMap* map = i.getNext();
	ConfigFile::SettingsMultiMap::iterator j;

	video_width_  = 1280;
	video_height_ = 1024;
	bVideo_fullscreen_ = true; 

	NameValuePairList list;
	String parameter,value;

	for (j = map->begin(); j != map->end(); ++j) {
		parameter = j->first;
		value = j->second;

		if(parameter.compare( "width" ) == 0)
			video_width_ = StringConverter::parseUnsignedInt( value );
		else if(parameter.compare( "height" ) == 0)
			video_height_ = StringConverter::parseUnsignedInt( value );
		else if(parameter.compare( "fullscreen" ) == 0)
			bVideo_fullscreen_ = (value.compare( "true" ) == 0) ? true : false;
		else
			list[j->first] = j->second;
	}

	renderWindow_ = root_->createRenderWindow( "BaseSoftware", 800, 600, false, &list );
}



void Application::initializeResources() 
{
	/////////////// In the end must be replaced by a single .zip recource load?
	ConfigFile cf;
	cf.load( "resources.ini" );
	ConfigFile::SectionIterator i = cf.getSectionIterator();

	String secName;
	while (i.hasMoreElements())
	{
		secName = i.peekNextKey();
		ConfigFile::SettingsMultiMap* map = i.getNext();
		ConfigFile::SettingsMultiMap::iterator j;
		for (j = map->begin(); j != map->end(); ++j)
			ResourceGroupManager::getSingleton().addResourceLocation(j->second, j->first, secName);
	}

	ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}



void Application::initializeCEGUI()
{
	ogreRenderer_ = &CEGUI::OgreRenderer::bootstrapSystem( *renderWindow_ );

	CEGUI::Font::setDefaultResourceGroup( "fonts" );
	CEGUI::ImageManager::setImagesetDefaultResourceGroup( "imagesets" );
	CEGUI::WindowManager::setDefaultResourceGroup( "layouts" );
	CEGUI::WidgetLookManager::setDefaultResourceGroup( "looknfeel" );
	CEGUI::Scheme::setDefaultResourceGroup( "schemes" );

	CEGUI::Window* rootWindow = CEGUI::WindowManager::getSingletonPtr()->createWindow( "DefaultWindow", "rootWindow" );
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( rootWindow );	
}
