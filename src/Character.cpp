
// including headers
#include "Character.h"



Character::Character( const String& name, const String& portret ) : Organism( name, portret ),
	player_control( true )
{
}



Character::~Character()
{
}



void Character::create( SceneManager* sceneManager, const String& ent_str, const Vector3& pos_vec )
{
	Organism::create( sceneManager, ent_str, pos_vec );		// calls the create function of the base class (Organism)
}