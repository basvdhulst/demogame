
// including headers
#include "CameraManager.h"



CameraManager::CameraManager() :
	camera_(0),
	viewport_(0),
	goto_pos_(0,0,0),
	camera_velocity_(0,0,0)
{
}



CameraManager::~CameraManager()
{
}



void CameraManager::create( SceneManager* sceneManager, RenderWindow* renderWindow )
{
	sceneManager_	= sceneManager;
	renderWindow_	= renderWindow;

	camera_		= sceneManager_->createCamera( "camera" );
	viewport_	= renderWindow_->addViewport( camera_ );
	groundNode	= sceneManager_->getRootSceneNode()->createChildSceneNode();
	cameraNode	= groundNode->createChildSceneNode( Vector3( 0, 300, 300 ) );
	cameraNode	->attachObject( camera_ );
	camera_		->lookAt( groundNode->getPosition() );
	goto_pos_	= groundNode->getPosition();
}



void CameraManager::update( float dtimeInSeconds )
{
	updateCameraPosition( dtimeInSeconds );
}



void CameraManager::updateCameraPosition( float dtimeInSeconds )
{
	Vector3 goto_diff = goto_pos_ - camera_->getPosition();

	if( goto_diff.squaredLength() > 1.0f )	// check if camera position is in range of goto_pos
	{
		camera_velocity_ += (goto_diff * 16.0f - 2 * camera_velocity_ * 4.0f) * dtimeInSeconds;	// NOTE: the 2 floats have to be x and sqrt(x) to make it critically damped!
		camera_->move( camera_velocity_ * dtimeInSeconds );
	}
}



Camera* CameraManager::getCamera()
{
	return camera_;
}



void CameraManager::moveCamera( Vector3 goto_pos )
{
	goto_pos_ = goto_pos;
}