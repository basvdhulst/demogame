
// including headers
#include <Ogre.h>
#include "Application.h"

// using namespaces
using namespace Ogre;

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN

INT WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
#else
int main( int argc, char *argv[] ) 
#endif
{
	// Create application object
    Application app;

	try {
		app.create();
		app.run();
	}
	catch(Exception& e) {
		#if OGRE_PLATFORM == PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBoxA( NULL, e.getFullDescription().c_str(), "Fatal error: ", MB_OK | MB_ICONERROR | MB_TASKMODAL );
		#else
			fprintf( stderr, "Fatal error: %s\n", e.getFullDescription().c_str() );
		#endif
	}

	return(0);
}