
// including headers
#include "DebugManager.h"

// singleton template
template<> DebugManager* Singleton< DebugManager >::ms_Singleton = 0;



DebugManager::DebugManager() :
	windowManager_(0),
	rootWindow_(0),
	debugWindow_(0),
	expiredTimeInSeconds_(0),
	timeSinceFPSUpdate_(0),
	framesSinceFPSUpdate_(0)
{
}



DebugManager::~DebugManager()
{
}



void DebugManager::create()
{
	windowManager_	= CEGUI::WindowManager::getSingletonPtr();
	rootWindow_		= windowManager_->getWindow( "rootWindow" );

	CEGUI::SchemeManager::getSingletonPtr()->create( "TaharezLook.scheme" );

	debugWindow_	= windowManager_->loadWindowLayout( "debug.layout" );
	rootWindow_		->addChildWindow( debugWindow_ );
}



void DebugManager::update( float dtimeInSeconds )
{
	if( timeSinceFPSUpdate_ >= 1.0f )
	{
		float FPS				= framesSinceFPSUpdate_/timeSinceFPSUpdate_;
		framesSinceFPSUpdate_	= 0;
		timeSinceFPSUpdate_		= 0;
		String FPS_str			= StringConverter::toString( FPS, 3 );
		windowManager_			->getWindow( "debug/FPSNum" )->setText( FPS_str );
	}

	timeSinceFPSUpdate_		+= dtimeInSeconds;
	expiredTimeInSeconds_	+= dtimeInSeconds;
	framesSinceFPSUpdate_	++;
}



void DebugManager::printToConsole( const String& str )
{
	OutputDebugString( str.c_str() );
	OutputDebugString( "\n" );
}