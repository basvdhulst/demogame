
// including headers
#include "StateManager.h"
#include "InputManager.h"



StateManager::StateManager() :
	renderSystem_(0),
	renderWindow_(0),
	inputManager_(0),
	currentState_(STARTUP)
{
}



StateManager::~StateManager() 
{
}



void StateManager::create( RenderSystem* renderSystem, RenderWindow* renderWindow, InputManager& inputManager )
{
	renderSystem_ = renderSystem;
	renderWindow_ = renderWindow;
	inputManager_ = &inputManager;
}



State StateManager::getCurrentState()
{
	return currentState_;
}



ushort StateManager::getScreenWidth()
{
	return renderWindow_->getWidth();
}



ushort StateManager::getScreenHeight()
{
	return renderWindow_->getHeight();
}



bool StateManager::isFullscreen()
{
	return renderWindow_->isFullScreen();
}



void StateManager::setVideoMode( bool bFullscreen, ushort width, ushort height )
{
	if( width && height )
	{
		videoWidth_ = width;
		videoHeight_ = height;
	}
	
	renderWindow_->setFullscreen( bFullscreen, videoWidth_, videoHeight_ );
	inputManager_->updateMouseWindow();
}



void StateManager::shutDown()
{
	requestStateChange( SHUTDOWN );
}



bool StateManager::requestStateChange(State newState) 
{
	if(newState == currentState_) {
		return false;
	}

	currentState_ = newState;
	return true;
}