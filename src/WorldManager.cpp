
// including headers
#include "WorldManager.h"
#include "GUIManager.h"
#include "StateManager.h"
#include "Character.h"



WorldManager::WorldManager() :
	root_(0),
	renderWindow_(0),
	guiManager_(0),
	stateManager_(0),
	sceneManager_(0),
	worldGrid_(0),
	gridLength_(9),
	gridWidth_(8),
	gridStep_(20.0f),
	gridNode_pointto_(0),
	gridNode_selection_(0),
	gridEnt_pointto_(0),
	gridEnt_selection_(0)
{
}



WorldManager::~WorldManager()
{
	// deletes the characters and empties the vector
	for( std::vector< Character* >::iterator it = team_vec_.begin(); it != team_vec_.end(); it++ )
		delete (*it);

	team_vec_.clear();		
}



void WorldManager::create( Root* root, RenderWindow* renderWindow, GUIManager& guiManager, StateManager& stateManager )
{
	root_			= root;
	renderWindow_	= renderWindow;
	guiManager_		= &guiManager;
	stateManager_	= &stateManager;
	sceneManager_	= root_->createSceneManager( ST_GENERIC, "sceneManager" );
	cameraManager_	.create( sceneManager_, renderWindow_ );

	loadWorldData( "media/terrains/world1.txt" );
	createWorldGeometry();
	createWorldGrid( gridLength_, gridWidth_, gridStep_ );
	//createWorldObstacles();
	
	// !!!!!!!!!!! TEST CENTER
	Character* player = new Character( "Bas", "portrets/man1" );
	player->create( sceneManager_, "cube.mesh", Vector3(0.5f*gridStep_, 0.75f*gridStep_, 0.5f*gridStep_) );
	player->setScale( Vector3(0.15f, 0.3f, 0.15f) );
	team_vec_.push_back( player );
	cameraManager_	.moveCamera( player->getPosition() );

	gridNode_pointto_	= sceneManager_->getRootSceneNode()->createChildSceneNode( Vector3::ZERO );
	gridNode_selection_	= sceneManager_->getRootSceneNode()->createChildSceneNode( player->getPosition() );

	Plane plane( Vector3::UNIT_Y, 0 );
	MeshManager::getSingletonPtr()->createPlane( "gridSquare", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, gridStep_, gridStep_, 1, 1, false, 1, 1, 1, Vector3::UNIT_Z );
	gridEnt_pointto_	= sceneManager_->createEntity( "gridSquare" );
	gridEnt_pointto_	->setMaterialName( "GUIImages/square_red" );
	gridEnt_selection_	= sceneManager_->createEntity( "gridSquare" );
	gridEnt_selection_	->setMaterialName( "GUIImages/cross1" );
	gridNode_selection_	->attachObject( gridEnt_selection_ );
}



void WorldManager::update( float dtimeInSeconds )
{
	cameraManager_.update( dtimeInSeconds );
}



void WorldManager::loadWorldData( const char* worldmap_fileName )
{
	std::ifstream mifstream( worldmap_fileName, std::ios_base::binary );
	if( mifstream.fail() )
		throw Exception( 1, "World data file not found", worldmap_fileName );

	FileStreamDataStream worldmap_file( &mifstream, false );

	while( worldmap_file.isReadable() && !worldmap_file.eof() )
	{
		String line = worldmap_file.getLine();
		worldMap_.push_back( StringUtil::split( line ) );
	}
}



void WorldManager::createWorldGeometry()
{
	Plane plane( Vector3::UNIT_Y, 0 );
	MeshManager::getSingletonPtr()->createPlane( "ground", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 180, 40, 1, 1, false, 1, 3, 1, Vector3::UNIT_Z );
	Entity* ground = sceneManager_->createEntity( "ground" );
	ground->setMaterialName( "terrains/railroad1" );
	sceneManager_->getRootSceneNode()->createChildSceneNode( Vector3( 90, 0, 80 ) )->attachObject( ground );
	MeshManager::getSingletonPtr()->createPlane( "ground2", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 180, 60, 1, 1, false, 1, 3, 1, Vector3::UNIT_Z );
	Entity* ground2 = sceneManager_->createEntity( "ground2" );
	ground2->setMaterialName( "terrains/water1" );
	sceneManager_->getRootSceneNode()->createChildSceneNode( Vector3( 90, 0, 130 ) )->attachObject( ground2 );
	MeshManager::getSingletonPtr()->createPlane( "ground3", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 180, 60, 1, 1, false, 1, 1, 0.33f, Vector3::UNIT_Z );
	Entity* ground3 = sceneManager_->createEntity( "ground3" );
	ground3->setMaterialName( "terrains/grass1" );
	sceneManager_->getRootSceneNode()->createChildSceneNode( Vector3( 90, 0, 30 ) )->attachObject( ground3 );
}



void WorldManager::createWorldGrid( ushort length, ushort width, float step )
{
 	MaterialPtr worldGrid_material	= MaterialManager::getSingleton().create( "worldGrid_material", ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME ); 
	worldGrid_material				->setReceiveShadows( false );
	worldGrid_material				->getTechnique( 0 )->getPass( 0 )->setAmbient( 0.8f, 0.8f, 0.8f );
 
	worldGrid_ = sceneManager_->createManualObject( "worldGrid" ); 
	worldGrid_->begin( "worldGrid_material", Ogre::RenderOperation::OT_LINE_LIST );

	for( short i = 0; i <= length; i++ )
	{
		worldGrid_->position( i*step, 0.1f, 0.0f );
		worldGrid_->position( i*step, 0.1f, width*step );
	}

	for( short i = 0; i <= width; i++ )
	{
		worldGrid_->position( 0.0f, 0.1f, i*step );
		worldGrid_->position( length*step, 0.1f, i*step  );
	}
	
	worldGrid_					->end();
	worldGrid_					->setRenderQueueGroup( 51 );		// 50 = default
	SceneNode* worldGrid_node	= sceneManager_->getRootSceneNode()->createChildSceneNode( "worldGrid_node" ); 
	worldGrid_node				->attachObject( worldGrid_ );
}



void WorldManager::updateShortestPath( const std::pair< short, short >* from_pos, const std::pair< short, short >* to_pos )
{
	// detach and remove entities for all sceneNodes, remove sceneNodes and then empty sceneNode vector
	std::vector< SceneNode* >::iterator iter;
	
	for( iter = shortestPath_SceneNodes_.begin(); iter != shortestPath_SceneNodes_.end(); iter++ )
	{
		sceneManager_->destroyMovableObject( (*iter)->detachObject( ushort(0) ) );
		sceneManager_->destroySceneNode( (*iter) );
	}
	
	shortestPath_SceneNodes_.clear();

	if( from_pos && to_pos ) {
		if( pathfinder_.findShortestPath( worldMap_, *from_pos, *to_pos, shortestPath_vec_ ) )
		{
			// fill sceneNode vector and attach entities
			SceneNode*	pathNode;
			Entity*		ent;
			std::vector< std::pair< short, short > >::iterator it;
			//iterate over the shortest path, but ommit the first and last entry
			for( it = ++shortestPath_vec_.begin(); it < shortestPath_vec_.end()-1; it++ )
			{
				pathNode = sceneManager_->getRootSceneNode()->createChildSceneNode();
				pathNode->setPosition( (it->first + 0.5f)*gridStep_, 0.1f, (it->second + 0.5f)*gridStep_ );
				ent = sceneManager_->createEntity( "gridSquare" );
				ent->setMaterialName( "GUIImages/square_green" );
				pathNode->attachObject( ent );
				shortestPath_SceneNodes_.push_back( pathNode );
			}
		}
	}
}



bool WorldManager::mouseLeftDownEvent()
{
	return true;
}



bool WorldManager::mouseRightDownEvent()
{
	return true;
}



bool WorldManager::mouseMovedEvent( float xPos, float yPos )
{
	Ray cameraRay = cameraManager_.getCamera()->getCameraToViewportRay( xPos/renderWindow_->getWidth(), yPos/renderWindow_->getHeight() );
	std::pair< bool, float > cameraRay_hit = cameraRay.intersects( Plane( Vector3::UNIT_Y, 0 ) );

	if( cameraRay_hit.first )
	{
		Vector3 cameraRay_hitPoint = cameraRay.getPoint( cameraRay_hit.second );
		Vector3 cameraRay_gridPointNorm, cameraRay_gridPoint;
		cameraRay_gridPointNorm.x = Ogre::Math::Floor( cameraRay_hitPoint.x / gridStep_ );
		cameraRay_gridPointNorm.y = 0;
		cameraRay_gridPointNorm.z = Ogre::Math::Floor( cameraRay_hitPoint.z / gridStep_ );
		cameraRay_gridPoint.x = (cameraRay_gridPointNorm.x + 0.5f) * gridStep_;
		cameraRay_gridPoint.y = cameraRay_gridPointNorm.y + 0.1f;
		cameraRay_gridPoint.z = (cameraRay_gridPointNorm.z + 0.5f) * gridStep_;
		
		// check if mouse is pointing within grid boundaries
		if( gridNode_pointto_->getPosition() != cameraRay_gridPoint &&
				cameraRay_gridPointNorm.x >= 0 && cameraRay_gridPointNorm.x < gridLength_ &&
					cameraRay_gridPointNorm.z >= 0 && cameraRay_gridPointNorm.z < gridWidth_ )
		{
			// locate gridEnt_pointto_ at the mouse position (snapped to grid) and make visible if not already
			gridNode_pointto_->setPosition( cameraRay_gridPoint );
			if( !gridEnt_pointto_->isAttached() )
				gridNode_pointto_->attachObject( gridEnt_pointto_ );

			// show the shortest path if a grid is selected and the mouse points to grid
			if( gridEnt_selection_->isAttached() && gridEnt_pointto_->isAttached() )
			{
				Vector3 from_vec = gridNode_selection_->getPosition();
				std::pair< short, short > from_pos;
				from_pos.first = static_cast< short >( Ogre::Math::Floor( from_vec.x / gridStep_ ) );
				from_pos.second = static_cast< short >( Ogre::Math::Floor( from_vec.z / gridStep_ ) );
				std::pair< short, short > to_pos;
				to_pos.first = static_cast< short >( cameraRay_gridPointNorm.x );
				to_pos.second = static_cast< short >( cameraRay_gridPointNorm.z );
				updateShortestPath( &from_pos, &to_pos );
			}
		}
	}

	return true;
}



bool WorldManager::mouseScrollEvent( short scroll )
{
	return true;
}



bool WorldManager::keyWPressedEvent()
{
	cameraManager_.moveCamera( cameraManager_.getCamera()->getPosition() + Vector3(10,0,0) );
	return true;
}



bool WorldManager::keyAPressedEvent()
{
	return true;
}



bool WorldManager::keySPressedEvent()
{
	return true;
}



bool WorldManager::keyDPressedEvent()
{
	return true;
}