
// including headers
#include "InputManager.h"
#include "GUIManager.h"
#include "WorldManager.h"
#include "StateManager.h"

// DEBUG
#include "DebugManager.h"



InputManager::InputManager() :
	renderWindow_(0),
	guiManager_(0),
	worldManager_(0),
	stateManager_(0),
	oisInputManager_(0),
	mouse_(0),
	keyboard_(0)
{
}



InputManager::~InputManager()
{
	if(mouse_)
		oisInputManager_->destroyInputObject(mouse_);
	if(keyboard_)
		oisInputManager_->destroyInputObject(keyboard_);

	OIS::InputManager::destroyInputSystem(oisInputManager_);
}



void InputManager::create( RenderWindow* renderWindow, GUIManager& guiManager, WorldManager& worldManager, StateManager& stateManager )
{
	renderWindow_	= renderWindow;
	guiManager_		= &guiManager;
	worldManager_	= &worldManager;
	stateManager_	= &stateManager;

	OIS::ParamList list;
	std::ostringstream windowHndStr;
	uint64 hWnd = 0;
	renderWindow_->getCustomAttribute( "WINDOW", &hWnd );
	windowHndStr << hWnd;
	list.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
	oisInputManager_ = OIS::InputManager::createInputSystem(list);

	mouse_		= static_cast<OIS::Mouse*>(oisInputManager_->createInputObject(OIS::OISMouse, true));
	keyboard_	= static_cast<OIS::Keyboard*>(oisInputManager_->createInputObject(OIS::OISKeyboard, true));
	mouse_		->setEventCallback(this);
	keyboard_	->setEventCallback(this);

	this->updateMouseWindow();

	// set-up default key-mapping
	for( short i = 0; i < MAX_KEYS_; i++ )
		keyMap_[i] = i;
}



void InputManager::update() 
{
	mouse_		->capture();
	keyboard_	->capture();
}



void InputManager::updateMouseWindow()
{
	if( mouse_ && renderWindow_ )
	{
		mouse_->getMouseState().width = renderWindow_->getWidth();
		mouse_->getMouseState().height = renderWindow_->getHeight();
	}
}



bool InputManager::mouseMoved( const OIS::MouseEvent &evt )
{
	if( static_cast<short>( evt.state.Z.rel ) )
	{
		guiManager_		->mouseScrollEvent( static_cast<short>( evt.state.Z.rel ) );
		worldManager_	->mouseScrollEvent( static_cast<short>( evt.state.Z.rel ) );
	}
	else
	{
		guiManager_		->mouseMovedEvent( static_cast<float>( evt.state.X.abs ), static_cast<float>( evt.state.Y.abs ) );
		worldManager_	->mouseMovedEvent( static_cast<float>( evt.state.X.abs ), static_cast<float>( evt.state.Y.abs ) );
	}

	return true;
}



bool InputManager::mousePressed( const OIS::MouseEvent &evt, OIS::MouseButtonID btn )
{
	if( btn == OIS::MB_Left )
	{
		guiManager_		->mouseLeftDownEvent();
		worldManager_	->mouseLeftDownEvent();
		return true;
	}

	if( btn == OIS::MB_Right )
	{
		guiManager_		->mouseRightDownEvent();
		worldManager_	->mouseRightDownEvent();
		return true;
	}

	return false;
}



bool InputManager::mouseReleased( const OIS::MouseEvent &evt, OIS::MouseButtonID btn )
{
	if( btn == OIS::MB_Left )
	{
		guiManager_->mouseLeftUpEvent();
		return true;
	}

	if( btn == OIS::MB_Right )
	{
		guiManager_->mouseRightUpEvent();
		return true;
	}

	return false;
}



bool InputManager::keyPressed( const OIS::KeyEvent &evt )
{
	// key out of bounds (array error)
	if( evt.key >= MAX_KEYS_ )
		return false;

	switch( keyMap_[evt.key] )
	{
		case OIS::KC_W:
			worldManager_->keyWPressedEvent();
			return true;
		case OIS::KC_A:
			worldManager_->keyAPressedEvent();
			return true;
		case OIS::KC_S:
			worldManager_->keySPressedEvent();
			return true;
		case OIS::KC_D:
			worldManager_->keyDPressedEvent();
			return true;
		case OIS::KC_F1:
		{
			guiManager_->setVideoMode( stateManager_->isFullscreen(), 800, 600 );
			String resolution_str = StringConverter::toString( stateManager_->getScreenWidth() ) + "x" + StringConverter::toString( stateManager_->getScreenHeight() );
			guiManager_->setWindowText( guiManager_->getWindowFromString( "debug/videoMode" ), resolution_str );
			guiManager_->showWindowInSeconds( guiManager_->getWindowFromString( "debug/videoMode" ), 3.0f );
			return true;
		}
		case OIS::KC_F2:
		{
			guiManager_->setVideoMode( stateManager_->isFullscreen(), 1280, 1024 );
			String resolution_str = StringConverter::toString( stateManager_->getScreenWidth() ) + "x" + StringConverter::toString( stateManager_->getScreenHeight() );
			guiManager_->setWindowText( guiManager_->getWindowFromString( "debug/videoMode" ), resolution_str );
			guiManager_->showWindowInSeconds( guiManager_->getWindowFromString( "debug/videoMode" ), 3.0f );
			return true;
		}
		case OIS::KC_F3:
		{
			guiManager_->setVideoMode( stateManager_->isFullscreen(), 1680, 1050 );
			String resolution_str = StringConverter::toString( stateManager_->getScreenWidth() ) + "x" + StringConverter::toString( stateManager_->getScreenHeight() );
			guiManager_->setWindowText( guiManager_->getWindowFromString( "debug/videoMode" ), resolution_str );
			guiManager_->showWindowInSeconds( guiManager_->getWindowFromString( "debug/videoMode" ), 3.0f );
			return true;
		}
		case OIS::KC_F12:
		{
			guiManager_->setVideoMode( !stateManager_->isFullscreen(), 0, 0 );
			String fullscreen_str = stateManager_->isFullscreen() ? "fullscreen" : "windowed";
			guiManager_->setWindowText( guiManager_->getWindowFromString( "debug/videoMode" ), fullscreen_str );
			guiManager_->showWindowInSeconds( guiManager_->getWindowFromString( "debug/videoMode" ), 3.0f );
			return true;
		}
		case OIS::KC_ESCAPE:
			guiManager_->openMainMenu();
			return true;
		default:
			return false;
	}
}



bool InputManager::keyReleased( const OIS::KeyEvent &evt )
{
	return true;
}