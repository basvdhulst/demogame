
// including headers
#include "Pathfinder.h"



Pathfinder::Pathfinder()
{
}



Pathfinder::~Pathfinder()
{
}



short Pathfinder::getMapValue( const std::vector< StringVector >& worldMap, const std::pair< short, short >& pos )
{
	short x = pos.first;
	short z = pos.second;

	if( x < 0 || z < 0 )
		return -1;

	if( worldMap.size() <= static_cast< ushort >(z) )
		return -1;
	if( worldMap.at(z).size() <= static_cast< ushort >(x) )
		return -1;
	
	String mapString = worldMap.at(z).at(x);

	if( StringConverter::isNumber( mapString ) )
		return static_cast< short >( StringConverter::parseInt( mapString ) );
	else
		return -1;
}



float Pathfinder::getDistance( const std::pair< short, short >& pos1, const std::pair< short, short >& pos2 )
{
	float pos1_x = static_cast< float > (pos1.first);
	float pos1_z = static_cast< float > (pos1.second);
	float pos2_x = static_cast< float > (pos2.first);
	float pos2_z = static_cast< float > (pos2.second);

	return sqrt( pow( pos1_x-pos2_x, 2 ) + pow( pos1_z - pos2_z, 2 ) );
}



std::vector< std::pair< short, short > >& Pathfinder::reconstruct_path(	const std::map< std::pair < short, short >, std::pair< short, short > >& camefrom,
																		const std::pair< short, short >& pos,
																		std::vector< std::pair< short, short > >& path_vec )
{
	if( camefrom.count( pos ) == 1 ) // pos has a camefrom-node
	{
		reconstruct_path( camefrom, camefrom.find( pos )->second, path_vec ).push_back( pos );
		return path_vec;
	}
	else
	{
		path_vec.push_back( pos );
		return path_vec;
	}
}




bool Pathfinder::findShortestPath(	const std::vector< StringVector >& worldMap, const std::pair< short, short >& start_pos,
									const std::pair< short, short >& goal_pos, std::vector< std::pair< short, short > >& path_vec )
{
	// make sure the path_vec is empty
	if( !path_vec.empty() )
		path_vec.clear();

	std::multimap< float, std::pair< short, short > > openset;
	std::multimap< std::pair< short, short >, float > closedset;
	std::map< std::pair< short, short >, std::pair< short, short > > camefrom;

	float f_score = getDistance( start_pos, goal_pos );
	openset.insert( std::pair< float, std::pair< short, short > >( f_score, start_pos ) );  // add start_pos to openset

	while( !openset.empty() )
	{
		const std::pair< short, short >* go_pos	= &openset.begin()->second; // gets node with the lowest f value
		float go_f_score						= openset.begin()->first;	// gets its f value

		// shortest path has been found?
		if( *go_pos == goal_pos ) {
			reconstruct_path( camefrom, goal_pos, path_vec );
			return true; // return true, it worked!
		}
		
		// add object to closedset and reassign the go_pos pointer to the object in closedset, whereafter the object is removed from openset
		go_pos = &closedset.insert( std::pair< std::pair< short, short >, float >( *go_pos, go_f_score ) )->first;
		openset.erase( openset.begin() );

		// handle neighbors
		for( short i = -1; i <= 1; i++ )
			for( short j = -1; j <= 1; j++ )
				if( i || j ) //i+j == 1 || i+j == -1 )		// conditional statement to control the step possibilities (e.g. with or without diagonal movement)
				{
					std::pair< short, short > neighbor_pos( go_pos->first+i, go_pos->second+j );
					short map_value = getMapValue( worldMap, neighbor_pos );	// check whether neighbor_pos is available on the map

					if( closedset.count( neighbor_pos ) == 0 && map_value >= 0) // only recalculate if neighbor_pos is not yet in closedset and point is on map
					{
						// g_score is the difficulty to travel from go_pos to the neighbor + the difficulty (g_score) of go_pos to the start_pos
						float new_g_score =	map_value*getDistance( *go_pos, neighbor_pos ) +
												(closedset.find( *go_pos )->second - getDistance( *go_pos, goal_pos ));

						bool update_g_score = true;
						std::multimap< float, std::pair< short, short > >::iterator it = openset.begin();

						while( it != openset.end() ) {
							if( (*it).second == neighbor_pos ) // found
							{
								if( (*it).first - getDistance( neighbor_pos, goal_pos ) > new_g_score ) // found, and new one is better
								{
									openset.erase( it );	// erase pos, will later be inserted
									camefrom.find( neighbor_pos )->second = *go_pos;
								}
								else
									update_g_score = false; // found, and new one is worse, don't update

								it = openset.end();	// prevents crashes with it pointing to random memory after erase
							}
							else
								it++;
						}
						
						if( update_g_score ) {
							f_score = new_g_score + getDistance( neighbor_pos, goal_pos );
							openset.insert( std::pair< float, std::pair< short, short > >( f_score, neighbor_pos ) );
							camefrom.insert( std::pair< std::pair< short, short >, std::pair< short, short > >( neighbor_pos, *go_pos ) );
						}
					}
		}
	}

	return false; // return false, it didn't work!
}