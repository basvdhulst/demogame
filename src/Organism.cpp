
// including headers
#include "Organism.h"



Organism::Organism( const String& name, const String& portret ) :
	entity_(0),
	sceneNode_(0),
	name_( name ),
	portret_( portret )
{
}



Organism::~Organism()
{
}



void Organism::create( SceneManager* sceneManager, const String& ent_str, const Vector3& pos_vec )
{
	entity_		= sceneManager->createEntity( ent_str );
	sceneNode_	= sceneManager->getRootSceneNode()->createChildSceneNode( pos_vec );
	sceneNode_	->attachObject( entity_ );
}



const String& Organism::getName()
{
	return name_;
}



const String& Organism::getPortret()
{
	return portret_;
}



const Vector3& Organism::getPosition()
{
	return entity_->getParentNode()->getPosition();
}



void Organism::setScale( const Vector3& scale_vec )
{
	sceneNode_->setScale( scale_vec );
}