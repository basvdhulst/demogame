
// including headers
#include "TimeManager.h"
//#include "DebugManager.h"
#include "StateManager.h"
#include "GUIManager.h"
#include "WorldManager.h"
#include "InputManager.h"



TimeManager::TimeManager() :
	root_(0),
	stateManager_(0),
	guiManager_(0),
	worldManager_(0),
	inputManager_(0),
	timer_(0)
	//debugManager_(0)
{
}



TimeManager::~TimeManager()
{
}



void TimeManager::create( Root* root, StateManager& stateManager, GUIManager& guiManager, WorldManager& worldManager, InputManager& inputManager )
{
	root_			= root;
	stateManager_	= &stateManager;
	guiManager_		= &guiManager;
	worldManager_	= &worldManager;
	inputManager_	= &inputManager;
	timer_			= root_->getTimer();
	//debugManager_	= DebugManager::getSingletonPtr();
}



void TimeManager::runRenderLoop()
{
	uint64 timeOld			= timer_->getMicroseconds();
	float dtimeInSeconds	= 0.017f; // 0.017 corresponds to FPS ~ 59
	uint64 timeNew;

	while(stateManager_->getCurrentState() != SHUTDOWN)
	{
		//debugManager_	->update( dtimeInSeconds );
		inputManager_	->update();
		guiManager_		->update( dtimeInSeconds );
		worldManager_	->update( dtimeInSeconds );
		root_			->renderOneFrame();

		timeNew			= timer_->getMicroseconds();
		dtimeInSeconds	= (timeNew - timeOld)/1000000.0f;
		timeOld			= timeNew;
	}
}
