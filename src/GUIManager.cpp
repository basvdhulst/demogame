
// including headers
#include "GUIManager.h"
#include "WorldManager.h"
#include "StateManager.h"

// REMOVE!!!
//#include "DebugManager.h"


GUIManager::GUIManager() :
	worldManager_(0),
	stateManager_(0),
	ceguiSystem_(0),
	windowManager_(0),
	rootWindow_(0),
	mainMenu_(0),
	gameplayMenu_(0)
{
}



GUIManager::~GUIManager()
{
}



void GUIManager::create( WorldManager& worldManager, StateManager& stateManager, ushort video_width, ushort video_height, bool bVideo_fullscreen )
{
	worldManager_	= &worldManager;
	stateManager_	= &stateManager;
	ceguiSystem_	= CEGUI::System::getSingletonPtr();
	windowManager_	= CEGUI::WindowManager::getSingletonPtr();
	rootWindow_		= windowManager_->createWindow( "DefaultWindow", "rootWindow" );

	// CEGUI test center CHANGE POSITION !!!!
	CEGUI::SchemeManager::getSingleton().createFromFile( "TaharezLook.scheme" );
	ceguiSystem_->getDefaultGUIContext().getMouseCursor().setDefaultImage( "TaharezLook/MouseArrow" );
	CEGUI::ImageManager::getSingletonPtr()->loadImageset( "portrets.imageset", "imagesets" );
	this->loadWindowLayouts();
	this->setVideoMode( bVideo_fullscreen, video_width, video_height );
}



void GUIManager::update( float dtimeInSeconds )
{
	this->updateTempWindows( dtimeInSeconds );
}



void GUIManager::updateTempWindows( float dtimeInSeconds )
{
	for( tempWindowsIt_ = tempWindows_.begin(); tempWindowsIt_ != tempWindows_.end(); )
	{
		(*tempWindowsIt_).second -= dtimeInSeconds;

		if( (*tempWindowsIt_).second <= 0 )
		{
			(*tempWindowsIt_).first->hide();
			tempWindows_.erase( tempWindowsIt_++ );
		}
		else
			++tempWindowsIt_;
	}
}



CEGUI::Window* GUIManager::getWindowFromString( const String& window_str )
{
	return rootWindow_->getChild( window_str );
}



bool GUIManager::isMouseVisible()
{
	return ceguiSystem_->getDefaultGUIContext().getMouseCursor().isVisible();
}



void GUIManager::loadWindowLayouts()
{
	// Main menu
	mainMenu_		= windowManager_->loadLayoutFromFile( "mainMenu.layout" );

	rootWindow_->getChild( "mainMenu/return" )->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &GUIManager::mainMenuReturnEvent, this ) );
	rootWindow_->getChild( "mainMenu/quit" )->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &GUIManager::mainMenuQuitEvent, this ) );

	rootWindow_->addChild( mainMenu_ );

	// Gameplay menu
	gameplayMenu_	= windowManager_->loadLayoutFromFile( "gameplayMenu.layout" );

	rootWindow_->addChild( gameplayMenu_ );
}



void GUIManager::openMainMenu()
{
	mainMenu_->show();
	mainMenu_->activate();
}



void GUIManager::setMousePosition( float xPos, float yPos )
{
	ceguiSystem_->getDefaultGUIContext().injectMousePosition( xPos, yPos );
}



void GUIManager::setMouseVisibility( bool bShow )
{
	ceguiSystem_->getDefaultGUIContext().getMouseCursor().setVisible( bShow );
}



void GUIManager::setVideoMode( bool bFullscreen, ushort width, ushort height )
{
	float width_old = stateManager_->getScreenWidth();
	float height_old = stateManager_->getScreenHeight();

	stateManager_->setVideoMode( bFullscreen, width, height );

	float width_new = stateManager_->getScreenWidth();
	float height_new = stateManager_->getScreenHeight();

	ceguiSystem_->notifyDisplaySizeChanged( CEGUI::Size<float>( width_new, height_new ) );
	
	float AR_scale = (width_new/height_new)/(width_old/height_old);

	for( CEGUI::WindowManager::WindowIterator it = windowManager_->getIterator(); !it.isAtEnd(); ++it )
	{
		(*it)->setWidth( (*it)->getWidth() * CEGUI::UDim( 1.0f, width_new/width_old/AR_scale ) );
		(*it)->setHeight( (*it)->getHeight() * CEGUI::UDim( 1.0f, height_new/height_old ) );
		(*it)->setXPosition( (*it)->getXPosition() * CEGUI::UDim( 1.0f, width_new/width_old/AR_scale ) );
		(*it)->setYPosition( (*it)->getYPosition() * CEGUI::UDim( 1.0f, height_new/height_old ) );
	}
}



void GUIManager::setWindowText( CEGUI::Window* window, const String& text )
{
	window->setText( text );
}



void GUIManager::showWindowInSeconds( CEGUI::Window* window, float secs )
{
	if( secs > 0 )
	{
		std::pair<std::map<CEGUI::Window*,float>::iterator,bool> element = tempWindows_.insert( std::pair<CEGUI::Window*,float>( window, secs ) );

		// if the element already existed, reset the time by hand
		if( element.second == false )
			(*element.first).second = secs;
	}

	window->show();
}



bool GUIManager::mouseLeftDownEvent()
{
	ceguiSystem_->getDefaultGUIContext().injectMouseButtonDown( CEGUI::LeftButton );
	return true;
}



bool GUIManager::mouseLeftUpEvent()
{
	ceguiSystem_->getDefaultGUIContext().injectMouseButtonUp( CEGUI::LeftButton );
	return true;
}



bool GUIManager::mouseRightDownEvent()
{
	return true;
}



bool GUIManager::mouseRightUpEvent()
{
	return true;
}



bool GUIManager::mouseMovedEvent( float xPos, float yPos )
{
	this->setMousePosition( xPos, yPos );
	return true;
}



bool GUIManager::mouseScrollEvent( short scroll )
{
	return true;
}



bool GUIManager::mainMenuReturnEvent( const CEGUI::EventArgs& args )
{
	mainMenu_->hide();
	return true;
}



bool GUIManager::mainMenuQuitEvent( const CEGUI::EventArgs& args )
{
	stateManager_->shutDown();
	return true;
}
